FROM node:18-alpine

COPY package.json package-lock.json ./

RUN npm install && npm install -g nodemon 

COPY .  .

EXPOSE 3000

CMD [ "nodemon","src/index.js" ]